; #AutoIt3Wrapper_Au3Check_Parameters=-d -w 1 -w 2 -w 3 -w- 4 -w 5 -w 6 -w 7
#include <Array.au3>
#include "..\_IniEx.au3"

Func Example()
	Local $iniFile = "setttings.ini"
	$str = _IniReadEx($iniFile, "general", "k", "not found", True)
	MsgBox(0, @error, $str)

	$arr = _IniReadEx($iniFile, "general", "k", "not found")
	_ArrayDisplay($arr)

	$arr = _IniReadEx($iniFile, "general", "ignoreTitle", "not found")
	_ArrayDisplay($arr)
EndFunc   ;==>Example
