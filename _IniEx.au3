; AU3Check settings
#AutoIt3Wrapper_Run_AU3Check=Y
#AutoIt3Wrapper_Au3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -w 7
#AutoIt3Wrapper_AU3Check_Stop_OnWarning=Y
#include-once

#include <Array.au3>

; #INDEX# =======================================================================================================================
; Title .........: _IniEx
; AutoIt Version : v3.3.14.5 or higher
; Language ......: English
; Description ...: Extend support more feauture for ini files
; Note ..........:
; Author(s) .....: ddnm102
; Remarks .......:
; ===============================================================================================================================


; #CURRENT# =====================================================================================================================
; _IniReadEx: Read key from ini file as the standard function but support for one key with multi line value
; ===============================================================================================================================


; #FUNCTION# ====================================================================================================================
; Name ..........: _IniReadEx
; Description ...: Read key from ini file as the standard function but support for one key with multi line value
; Syntax ........: _IniReadEx($sFilePath, $sSection, $sKey, $sDefault[, $bRetStr = False[, $sDetermine = "|"]])
; Parameters ....: $sFilePath           - a string value.
;                  $sSection            - a string value.
;                  $sKey                - a string value.
;                  $sDefault            - a string value.
;                  $bRetStr             - [optional] a boolean value. Default is False.
;                  $sDetermine          - [optional] a string value. Default is "|".
; Return values .: String or Array
; Author ........: ddnm102
; Modified ......:
; Remarks .......: Return an Array as default, if you set $bRetStr = True, it will return the String. For example, Ini file with content below
;					[general]
;					keyName = value0
;					keyName.1 = value1
;					keyName.2 = value2
;					=> _IniReadEx(iniFile, "general", "keyName", "not found") => [value0, value1, value2]
;					=> _IniReadEx(iniFile, "general", "keyName", "not found", True) => value0|value1|value2
;=> if return as string: value0|value1|value2
; Related .......:
; Link ..........:
; Example .......: Yes
; ===============================================================================================================================
Func _IniReadEx($sFilePath, $sSection, $sKey, $sDefault, $bRetStr=False, $sDetermine="|")
	SetError(0)
	Local $sectionValue = IniReadSection($sFilePath, $sSection)
	;_ArrayDisplay($sectionValue)
	Local $testKey, $testValue
	Local $ret = IniRead($sFilePath, $sSection, $sKey, "__not__found__")
	;MsgBox(0,$key, $ret)
	If $ret == "__not__found__" Then $ret = ""
	$ret &= $sDetermine
	Local $idx = 0
	For $i = 1 To $sectionValue[0][0]
		$testKey = $sKey&"."&$i
		$idx = _ArraySearch($sectionValue, $testKey)
		If $idx <> -1 Then
			$testValue = $sectionValue[$idx][1]
			;MsgBox(0,$testKey, $testValue)
			$ret &= $testValue & $sDetermine
		EndIf
	Next
	If $ret == $sDetermine Then Return $sDefault
	$ret = StringTrimRight($ret, StringLen($sDetermine)) ; remove | from the right of $ret string
	If Not $bRetStr Then Return StringSplit($ret, $sDetermine, 1)
	Return $ret
EndFunc
